# SMSReceiver로 TP-SCTS(TP Service Centre Time Stamp) 값 확인

Android Studio에서 실행 시킨 후 SMS 보내면 log 찍힘.

참고한 블로그 들어가서 확인 잘 설명되어있음.

참고

[https://ju-hy.tistory.com/50](https://ju-hy.tistory.com/50)

[https://stackoverflow.com/questions/19681290/android-get-sms-delivery-time?rq=1](https://stackoverflow.com/questions/19681290/android-get-sms-delivery-time?rq=1)
