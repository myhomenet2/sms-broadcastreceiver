package com.example.myapplication;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

import java.util.Date;

public class SMSReceiver extends BroadcastReceiver {

    private static final String TAG = "SMSReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive() called");

        Bundle bundle = intent.getExtras();

        Object[] messages=(Object[])bundle.get("pdus");
//        SmsMessage[] messages = parseSmsMessage(bundle);
        SmsMessage[] sms=new SmsMessage[messages.length];

//        if(messages.length > 0){
//            String sender = messages[0].getOriginatingAddress();
//            String content = messages[0].getMessageBody().toString();
//            Date date = new Date(messages[0].getTimestampMillis());

        for(int n=0;n<messages.length;n++){
            sms[n]=SmsMessage.createFromPdu((byte[]) messages[n]);
            String sender = sms[0].getOriginatingAddress();
            String content = sms[0].getMessageBody().toString();
            // TP-SCTS(TP Service Centre Time Stamp) 값
            Date date = new Date(sms[0].getTimestampMillis());

            Log.d(TAG, "sender: " + sender);
            Log.d(TAG, "content: " + content);
            Log.d(TAG, "date: " + date);
        }
    }

    private SmsMessage[] parseSmsMessage(Bundle bundle){
        // PDU: Protocol Data Units
        Object[] objs = (Object[]) bundle.get("pdus");
        SmsMessage[] messages = new SmsMessage[objs.length];

        for(int i=0; i<objs.length; i++){
            messages[i] = SmsMessage.createFromPdu((byte[])objs[i]);
        }

        return messages;
    }

}